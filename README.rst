Simple Redirect
***************

Development
===========

Ensure you have your personal development settings file::

  ls -sl settings/dev_<your user name>.py

If this file does not exist::

  cp settings/dev_patrick.py settings/dev_<your user name>.py

Virtual Environment
-------------------

Linux virtual environment::

  virtualenv --python=python3 venv-simple-redirect
  # or
  python3 -m venv venv-simple-redirect

  source venv-simple-redirect/bin/activate

  pip install -r requirements/local.txt

::

  python3 -m venv venv-simple-redirect

Testing
-------

::

  find . -name '*.pyc' -delete
  py.test -x

Usage
-----

::
  ./init_dev.sh

Browse to http://localhost:8000/::

  user          staff
  password      letmein

Release and Deploy
==================

https://www.kbsoftware.co.uk/docs/
