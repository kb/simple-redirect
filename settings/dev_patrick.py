# -*- encoding: utf-8 -*-
from .local import *

# No need for a database
DATABASES = {}

MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)

INSTALLED_APPS += ("django_extensions", "debug_toolbar")

# force the debug toolbar to be displayed
def show_toolbar(request):
    return True


DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": show_toolbar,
    "INTERCEPT_REDIRECTS": False,
    "ENABLE_STACKTRACES": True,
}
