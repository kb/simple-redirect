from .local import *

# No need for a database
DATABASES = {}

# http://docs.celeryproject.org/en/2.5/django/unit-testing.html
BROKER_BACKEND = "memory"
CELERY_ALWAYS_EAGER = True
