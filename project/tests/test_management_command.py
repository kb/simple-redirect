# -*- encoding: utf-8 -*-
from django.core.management import call_command


def test_demo_data_project():
    call_command("init_project")
    call_command("demo_data_project")


def test_init_project():
    call_command("init_project")
