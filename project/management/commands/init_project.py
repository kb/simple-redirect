# -*- encoding: utf-8 -*-
"""
This command is designed to be run multiple times.  It can clear out data, and
then re-insert e.g. for setting up the main menu navigation.
"""
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = "Set-up project (e.g. main navigation)"

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        self.stdout.write("{} - Complete".format(self.help))
