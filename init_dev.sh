#!/bin/bash
# treat unset variables as an error when substituting.
set -u
# exit immediately if a command exits with a nonzero exit status.
set -e

django-admin.py init_project
django-admin.py demo_data_project
django-admin.py runserver 0.0.0.0:8000
