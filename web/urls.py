# -*- encoding: utf-8 -*-
from django.conf.urls import url

from .views import HomeRedirectView


urlpatterns = [
    url(r"^.*$", view=HomeRedirectView.as_view(), name="project.home")
]
