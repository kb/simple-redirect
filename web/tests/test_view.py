# -*- encoding: utf-8 -*-
from django.conf import settings
from django.urls import reverse
from http import HTTPStatus


def test_dash(client):
    response = client.get(reverse("project.dash"))
    assert HTTPStatus.FOUND == response.status_code
    assert settings.REDIRECT_URL == response.url


def test_home(client):
    response = client.get(reverse("project.home"))
    assert HTTPStatus.FOUND == response.status_code
    assert settings.REDIRECT_URL == response.url


def test_settings(client):
    response = client.get(reverse("project.settings"))
    assert HTTPStatus.FOUND == response.status_code
    assert settings.REDIRECT_URL == response.url
