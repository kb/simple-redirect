# -*- encoding: utf-8 -*-
from django.conf import settings
from django.views.generic.base import RedirectView


class HomeRedirectView(RedirectView):

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        return settings.REDIRECT_URL
