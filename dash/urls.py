# -*- encoding: utf-8 -*-
from django.conf.urls import url

from .views import DashView


urlpatterns = [
    url(regex=r"^$", view=DashView.as_view(), name="login"),
    url(regex=r"^$", view=DashView.as_view(), name="project.dash"),
    url(regex=r"^$", view=DashView.as_view(), name="project.settings"),
]
