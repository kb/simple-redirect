source venv-simple-redirect/bin/activate.fish
if command -q k3d
  echo "Using Kubernetes"
  set -x DATABASE_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x DATABASE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
  echo "KUBECONFIG:" $KUBECONFIG
else
  set -x DATABASE_HOST ""
  set -x DATABASE_PORT "5432"
end

set -x DATABASE_NAME "dev_app_simple_redirect_$USER"
set -x DATABASE_PASS ""
set -x DATABASE_USER "postgres"
set -x DJANGO_SETTINGS_MODULE "settings.dev_patrick"
set -x DOMAIN "redirect.kbsoftware.co.uk"
set -x HOST_NAME "http://localhost/"
set -x REDIRECT_URL "https://www.kbsoftware.co.uk"
set -x SECRET_KEY "the_secret_key"

source .private

echo "DATABASE_HOST:" $DATABASE_HOST
echo "DATABASE_NAME:" $DATABASE_NAME
echo "DATABASE_PASS:" $DATABASE_PASS
echo "DATABASE_PORT:" $DATABASE_PORT
echo "DJANGO_SETTINGS_MODULE:" $DJANGO_SETTINGS_MODULE
